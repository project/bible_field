<?php

/**
 * @file
 * Contains \Drupal\bible_field\BibleService
 */
 
namespace Drupal\bible_field;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * BibleService Class.
 */
class BibleService extends Dbt {
  
  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   * The config factory.
   */
  protected $configFactory;
  
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    
    parent::__construct($this->configFactory->get('bible_field.dbt')->get('apikey'));
  }
  
  
}