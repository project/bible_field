<?php

/**
 * @file
 * Contains \Drupal\bible_field\BibleFieldConfigForm
 */

namespace Drupal\bible_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure hello settings for this site.
 */
class BibleFieldConfigForm extends ConfigFormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bible_field_config';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bible_field.dbt',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bible_field.dbt');

    $dbt_url = Url::fromUri('http://www.digitalbibleplatform.com/');
    $form['apikey'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Digital Bible Platform API Key'),
      '#description' => $this->t('You can register for a key !link', ['!link' => \Drupal::l($this->t('here'), $dbt_url)]),
      '#default_value' => $config->get('apikey'),
    );  

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('bible_field.dbt')
      ->set('apikey', $form_state->getValue('apikey'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
?>