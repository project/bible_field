<?php

/**
 * @file
 * Contains \Drupal\bible_field\Plugin\Field\FieldWidget\BibleReferenceSelect.
 */
 
namespace Drupal\bible_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * Field widget for the bible field.
 *
 * @FieldWidget(
 *   id = "bible_field_select",
 *   label = @Translation("Select"),
 *   field_types = {
 *     "bible_field"
 *   },
 * )
 */
class BibleReferenceSelect extends WidgetBase {
  
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];
    $input = $form_state->getUserInput();
    $input = NestedArray::getValue($input, $element['#field_parents'] + [$this->fieldDefinition->getName(), $delta]);

    $element += [
      '#type' => 'container',
      '#title' => t('Bible Reference'),
      '#tree' => TRUE,
      '#attributes' => ['class' => ['biblefield-field-wrapper']],
    ];
    
    $element['language'] = [
      '#type' => 'value',
      '#value' => $item->allowedLanguages(),
    ];
    
    $allowed_versions = $item->allowedVersionOptions();
    $version = $item->version;
    if (!empty($input['version'])) {
      $version = $input['version'];
    }
    
    $book_input_id = implode('-', $element['#field_parents'] + [$this->fieldDefinition->getName(), $delta, 'book-select']);
    if (count($allowed_versions) == 1) {
      $version = reset(array_keys($allowed_versions));
      $element['version'] = [
        '#type' => 'value',
        '#value' => $version,
      ];
    }
    else {
      $element['version'] = array(
        '#type' => 'select',
        '#title' => t('Version'),
        '#empty_option' => t('- Select -'),
        '#options' => $allowed_versions,
        '#default_value' => $version,
        '#ajax' => [
          'wrapper' => $book_input_id,
          'method' => 'replace',
          'callback' => [$this, 'bookSelectAjax'],
         ]
      );
    }
    
    // Get the volumn code.
    $book_options = [];
    if ($version) {
      $volumes = json_decode($item->bibleService()->getJsonResponse(
        'library',
        'volume',
        [
          'language_code' => $item->allowedLanguages(),
          'version_code' => $version,
          'status' => 'live',
          'media' => 'text',
        ]
      ));
          
      $book_volume_map = [];
      foreach ($volumes as $volume) {
        $book_options[$volume->collection_name] = [];
        $books = json_decode($item->bibleService()->getJsonResponse(
          'library',
          'book',
          [
            'dam_id' => $volume->dam_id,
          ]
        ));
        foreach ($books as $book) {
          $book_volume_map[$book->book_id] = $volume->dam_id;
          $book_options[$volume->collection_name][$book->book_id] = $book->book_name;
        }
      }
    }
    
    $chapter_input_id = implode('-', $element['#field_parents'] + [$this->fieldDefinition->getName(), $delta, 'chapter-select']);
    $element['book'] = [
      '#type' => 'select',
      '#options' => $book_options,
      '#title' => t('Book'),
      '#empty_option' => t('- Select -'),
      '#default_value' => $item->book,
      '#ajax' => [
        'wrapper' => $chapter_input_id,
        'method' => 'replace',
        'callback' => [$this, 'chapterSelectAjax'],
      ],
      '#prefix' => '<div id="'.$book_input_id.'">',
      '#suffix' => '</div>',
    ];
       
    // Get the default book.
    $book = $item->book;
    if (!empty($input['book'])) {
      $book = $input['book'];
    }
    
    // Get the chapters for the book.
    $chapter_options = [];
    if ($book) {
      $chapters = json_decode($item->bibleService()->getJsonResponse(
        'library',
        'chapter',
        [
          'dam_id' => substr($volume->dam_id, 0, 6),
          'book_id' => $book,
        ]
      ));
      
      foreach ($chapters as $chapter) {
        $chapter_options[$chapter->chapter_id] = $chapter->chapter_id;
      }
    }

    $verse_input_id = implode('-', $element['#field_parents'] + [$this->fieldDefinition->getName(), $delta, 'verse-select']);
    $element['chapter'] = [
      '#type' => 'select',
      '#title' => t('Chapter'),
      '#options' => $chapter_options,
      '#prefix' => '<div id="'.$chapter_input_id.'">',
      '#suffix' => '</div>',
      '#default_value' => $item->chapter,
      '#ajax' => [
        'wrapper' => $verse_input_id,
        'method' => 'replace',
        'callback' => [$this, 'verseSelectAjax'],
      ]
    ];
    
    $chapter = $item->chapter;
    if (!empty($input['chapter'])) {
      $chapter = $input['chapter'];
    }
    
    // Get the verse options.
    $verse_options = [];
    if ($book && $chapter) {
      $verses = json_decode($item->bibleService()->getJsonResponse(
        'library',
        'verseinfo',
        [
          'dam_id' => $book_volume_map[$book],
          'book_id' => $book,
          'chapter_id' => $chapter,
        ]
      ));
      
    foreach ($verses->{$book}->{$chapter} as $verse) {
        $verse_options[$verse] = $verse;
      }
    }
    
    $element['verse_start'] = [
      '#type' => 'select',
      '#title' => t('Verse'),      
      '#empty_option' => '-',
      '#options' => $verse_options,
      '#prefix' => '<div id="'.$verse_input_id.'">',
      '#default_value' => $item->verse_start,
    ];
    $element['verse_end'] = [
      '#type' => 'select',
      '#title' => '',
      '#empty_option' => '-',
      '#options' => $verse_options,
      '#default_value' => $item->verse_end,
      '#field_prefix' => '-',
      '#suffix' => '</div>',
    ];
        
    return $element;
  }
  
  /**
   * Ajax Callback to Return Book input.
   */
  public function bookSelectAjax(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $array_parents = $element['#array_parents'];
    array_pop($array_parents);
    array_push($array_parents, 'book');
    return NestedArray::getValue($form, $array_parents);
  }
  
  /**
   * Ajax Callback to Return Chapter input.
   */
  public function chapterSelectAjax(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $array_parents = $element['#array_parents'];
    array_pop($array_parents);
    array_push($array_parents, 'chapter');
    return NestedArray::getValue($form, $array_parents);
  }
  
  /**
   * Ajax Callback to Return Verse inputs.
   */
  public function verseSelectAjax(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $array_parents = $element['#array_parents'];
    array_pop($array_parents);
    $inputs = NestedArray::getValue($form, $array_parents);
    return [
      'verse_start' => $inputs['verse_start'],
      'verse_end' => $inputs['verse_end'],
    ];
  }
}
