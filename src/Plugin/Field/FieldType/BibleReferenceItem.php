<?php

/**
 * @file
 * Contains \Drupal\bible_field\Plugin\Field\FieldType\BibleReferenceItem.
 */
 
namespace Drupal\bible_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\bible_field\Dbt;

/**
 * Plugin implementation of the 'bible_field' field type.
 *
 * @FieldType(
 *  id = "bible_field",
 *  label = @Translation("Bible Reference"),
 *  description = @Translation("Store Bible References"),
 *  default_widget = "bible_field_select",
 *  default_formatter = "bible_field_default"
 * )
 */
class BibleReferenceItem extends FieldItemBase {
  
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['language'] = DataDefinition::create('string')
      ->setLabel(t('Language Code'))
      ->setRequired(TRUE);
      
    $properties['language_name'] = DataDefinition::create('string')
      ->setLabel(t('Language'));

    $properties['version'] = DataDefinition::create('string')
      ->setLabel(t('Version Code'))
      ->setRequired(TRUE); 

    $properties['version_name'] = DataDefinition::create('string')
      ->setLabel(t('Version'));   

    $properties['volume'] = DataDefinition::create('string')
      ->setLabel(t('Volume Code'));  

    $properties['book'] = DataDefinition::create('string')
      ->setLabel(t('Book Abbreviation'))
      ->setRequired(TRUE);

    $properties['book_name'] = DataDefinition::create('string')
      ->setLabel(t('Book'));
    
    $properties['chapter'] = DataDefinition::create('integer')
      ->setLabel(t('Chapter'));
      
    $properties['verse_start'] = DataDefinition::create('string')
      ->setLabel(t('Starting Verse'));
      
    $properties['verse_end'] = DataDefinition::create('string')
      ->setLabel(t('Ending Verse'));
      
    $properties['ref_weight_start'] = DataDefinition::create('float')
      ->setLabel(t('Reference Weight Start'));
      
    $properties['ref_weight_end'] = DataDefinition::create('float')
      ->setLabel(t('Reference Weight End'));
    
    return $properties;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $definition) {
    return [
      'columns' => [
        'language' => [
          'type' => 'varchar',
          'length' => 3,
        ],
        'language_name' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'version' => [
          'type' => 'varchar',
          'length' => 3,
        ],
        'version_name' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'volume' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'book' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'book_name' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'chapter' => [
          'type' => 'int',
        ],
        'verse_start' => [
          'type' => 'varchar',
          'length' => 5,
        ],
        'verse_end' => [
          'type' => 'varchar',
          'length' => 5,
        ],
        'ref_weight_start' => [
          'type' => 'float',
        ],
        'ref_weight_end' => [
          'type' => 'float',
        ],
      ],
      'indexes' => [],
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'language' => 'ENG',
      'versions' => [
        'ESV',
      ]
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (!$this->book) {
      return TRUE;
    }
    return FALSE;
  }
  
  /**
   * {@inheritdoc}
   */
  public function presave() {
    // Fill in the human readable names.
    // Language
    $languages = json_decode($this->bibleService()->getJsonResponse('library', 'language', ['code' => $this->language]));
    $this->language_name = reset($languages)->language_name;
    
    $versions = json_decode($this->bibleService()->getJsonResponse('library', 'version', ['code' => $this->version]));
    $this->version_name = reset($versions)->version_name;
    
    // Get the volume dam_id
    $volumes = json_decode($this->bibleService()->getJsonResponse(
      'library',
      'volume',
      [
        'language_code' => $this->language,
        'version_code' => $this->version,
        'status' => 'live',
        'media' => 'text',
      ]
    ));
    
    $book_names = [];
    $book_volume_map = [];
    foreach ($volumes as $volume) {
      $volumes[$volume->dam_id] = $volume;
      $books = json_decode($this->bibleService()->getJsonResponse(
        'library',
        'book',
        [
          'dam_id' => $volume->dam_id,
        ]
      ));
      foreach ($books as $book) {
        $book_volume_map[$book->book_id] = $volume->dam_id;
        $book_names[$book->book_id] = $book->book_name;
        $book_order[$book->book_id] = $book->book_order;
      }
    }
    
    $this->volume = $book_volume_map[$this->book];
    $this->book_name = $book_names[$this->book];
    
    // Work out the ref weights.
    $ref_weight = "{$book_order[$this->book]}.".sprintf("%'.03d", $this->chapter);
    $this->ref_weight_start = (double) ($ref_weight . sprintf("%'.03d", $this->verse_start));
    $this->ref_weight_end = (double) ($ref_weight . sprintf("%'.03d", $this->verse_end));    
  }
  
  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $input = $form_state->getUserInput();
    $language = !empty($input['settings']['language']) ? $input['settings']['language'] : $this->getSetting('language');
    
    $element['language'] = [
      '#type' => 'select',
      '#title' => t('Language'),
      '#options' => $this->languageOptions(),
      '#default_value' => $language,
      '#ajax' => [
        'callback' => [$this, 'versionSelectAjax'],
        'wrapper' => 'bible-field-allowed-versions',
        'method' => 'replace',
        'progress' => [
          'type' => 'throbber',
          'message' => t('Loadng Versions...'),
        ],
      ],
      '#limit_validation_errors' => [
        ['settings','language'],
      ]
    ];
    
    if (!empty($language)) {
      $options = $this->versionOptions($language);
      $versions = $this->getSetting('versions');
      
      $element['versions'] = [
        '#type' => 'checkboxes',
        '#title' => t('Versions'),
        '#options' => $this->versionOptions($language),
        '#default_value' => $this->getSetting('versions'),
        '#prefix' => '<div id="bible-field-allowed-versions">',
        '#suffix' => '</div>',
      ];
    }
    else {
      $element['language']['#suffix'] = '<div class="bible-field-allowed-versions"></div>';
    }
    
    return $element;
  }
  
  /**
   * Ajax callback.
   */
  public function versionSelectAjax(array &$form, FormStateInterface $form_state) {
    return $form['settings']['versions'];
  }
 
  /**
   * Get the Dbt class.
   */
  public function bibleService() {
    return \Drupal::service('bible_field.bible');
  }
  
  /**
   * Get the language options from bbt.
   */
  public function languageOptions() {
    $languages = json_decode($this->bibleService()->getJsonResponse('library', 'language', []), TRUE);
    $options = [];
    foreach ($languages as $language) {
      $options[$language['language_code']] = $language['language_name'];
    }
    return $options;
  }
  
  /**
   * Get the allowed languages
   */
  public function allowedLanguages() {
    return $this->getSetting('language');
  }
  
  /**
   * Get the version options from Dbt.
   */
  public function versionOptions($language) {
    $versions = json_decode($this->bibleService()->getJsonResponse('library', 'volume', ['language_code' => $language, 'media' => 'text', 'status' => 'live']), true);
    $options = [];
    foreach ($versions as $version) {
      $options[$version['version_code']] = $version['version_name'];
    }
    return $options;
  }
  
  /**
   * Allowed versions.
   */
  public function allowedVersions() {
    return array_filter($this->getSetting('versions'));
  }
  
  /**
   * Get the allowed version options.
   */
  public function allowedVersionOptions() {
    $versions = $this->versionOptions($this->getSetting('language'));
    foreach ($versions as $code => $name) {
      if (!in_array($code, $this->allowedVersions())) {
        unset($versions[$code]);
      }
    }
    
    return $versions;
  }
  
  /**
   * Get the book options.
   */
  public function bookOptions($volume) {
    
  }
}
 