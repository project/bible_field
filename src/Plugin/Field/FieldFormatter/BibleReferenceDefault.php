<?php

/**
 * @file
 * Contains \Drupal\bible_field\Plugin\Field\FieldFormatter\BibleReferenceDefault.
 */
 
namespace Drupal\bible_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Field formatter for the bible field.
 *
 * @FieldFormatter(
 *   id = "bible_field_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "bible_field"
 *   },
 * )
 */
class BibleReferenceDefault extends FormatterBase {
  
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    
    foreach ($items as $delta => $item) {
      // Build Message
      // @todo: replace with twig template.
      $message = "{$item->book_name} {$item->chapter}";
      if ($item->verse_start) {
        $message .= ':'.$item->verse_start;
      }
      if ($item->verse_end) {
        $message .= '-'.$item->verse_end;
      }
      $message .= ' ('.$item->version.')';
      
      $element = [
        '#type' => 'markup',
        '#markup' => $message,
      ];
      $elements[$delta] = $element;
    }

    return $elements;
  }
  
}
