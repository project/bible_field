<?php

/**
 * @file
 * Contains \Drupal\bible_field\Plugin\Field\FieldFormatter\BibleReferenceText.
 */
 
namespace Drupal\bible_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Field formatter for the bible field.
 *
 * @FieldFormatter(
 *   id = "bible_field_text",
 *   label = @Translation("Passage Text"),
 *   field_types = {
 *     "bible_field"
 *   },
 * )
 */
class BibleReferenceText extends FormatterBase {
  
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    
    foreach ($items as $delta => $item) {
      // Build Passage
      // @todo: replace with twig template.
      $params = [
        'dam_id' => $item->volume,
        'book_id' => $item->book,
        'chapter_id' => $item->chapter,
      ];
      if ($item->verse_start) {
        $params['verse_start'] = $item->verse_start;
        if ($item->verse_end) {
          $params['verse_end'] = $item->verse_end;
        }
      }
      $verses = json_decode($item->bibleService()->getJsonResponse(
        'text',
        'verse',
        $params
      ));
      
      $passage = '<p>';
      $paragraph_id = NULL;
      foreach ($verses as $verse) {
        if (!$paragraph_id) {
          $paragraph_id = $verse->paragraph_number;
        }
        if ($verse->paragraph_number != $paragraph_id) {
          $paragraph_id = $verse->paragraph_number;
          $passage .= '</p><p>';
        }
        $passage .= $verse->verse_text;
      }
      $passage .= '</p>';
      
      // Reference
      $message = "{$item->book_name} {$item->chapter}";
      if ($item->verse_start) {
        $message .= ':'.$item->verse_start;
      }
      if ($item->verse_end) {
        $message .= '-'.$item->verse_end;
      }
      $message .= ' ('.$item->version.')';
  
      $element = [
        '#type' => 'markup',
        '#markup' => $passage . '- <small>'.$message.'</small>',
      ];
      $elements[$delta] = $element;
    }

    return $elements;
  }
  
}
