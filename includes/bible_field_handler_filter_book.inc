<?php

/**
 * A book filter - pulls in a select input with the currently selected bible CSV
 */
class bible_field_handler_filter_book extends views_handler_filter_string {
  function value_form(&$form, &$form_state) {
    $field = content_fields($this->definition['content_field_name']);
    //$options = array_merge(array('' => ''), bible_field_book_options($field['version']));
    $options = bible_field_book_options($field['version']);
    
    if ($form_state['form_key'] == 'config-item') { // It's a config form element
      $options = array('All' => '') + $options; // Add a blank option to the select for 'All'
    }
    parent::value_form(&$form, &$form_state);
    $class = str_replace($this->definition['content_field_name'] .'_', '', $this->definition['field']);
    $form['value'] = array(
      '#type' => 'select',
      '#title' => t('Book'),
      '#attributes' => array('class' => 'bible_field_book_filter'),
      '#options' => $options,
      '#default_value' => $this->value,
    );
  }
  
  function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      // @TODO: I probably shouldn't be setting UCASE like this
      $this->{$info[$this->operator]['method']}($field, 'UCASE');
    }
  }
}